#!/bin/bash

docker run -d --name nginx-1 -v /tmp:/usr/share/nginx/html nginx:latest
docker run -d --name nginx-2 -v /tmp:/usr/share/nginx/html nginx:latest

docker run -d --name nano-editor -v /tmp:/opt ex-01 sh -c "echo 'Olá mundo!' > /opt/index.html"
