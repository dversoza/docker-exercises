# Instructions to use nano

If you are going to use nano interactively, execute the container in interactive-terminal mode running the following command:

``` docker
    docker build -t ex_01 .
    docker run -it ex_01
```
